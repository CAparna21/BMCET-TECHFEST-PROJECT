-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 19, 2018 at 04:51 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id4308625_aayam`
--
CREATE DATABASE IF NOT EXISTS `id4308625_aayam` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `id4308625_aayam`;

-- --------------------------------------------------------

--
-- Table structure for table `aayam`
--

CREATE TABLE `aayam` (
  `INDX` int(10) NOT NULL,
  `NAME1` varchar(240) COLLATE utf8_unicode_ci NOT NULL,
  `NAME2` varchar(240) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME3` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EVENT` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `PHNNO` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `COLLEGE` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `RESULT` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LEVEL` int(4) DEFAULT NULL,
  `STATUS` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VENUE` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIME` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aayam`
--


--
-- Indexes for dumped tables
--

--
-- Indexes for table `aayam`
--
ALTER TABLE `aayam`
  ADD PRIMARY KEY (`INDX`),
  ADD UNIQUE KEY `EVENT` (`EVENT`,`PHNNO`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aayam`
--
ALTER TABLE `aayam`
  MODIFY `INDX` int(10) NOT NULL AUTO_INCREMENT;
COMMIT;


